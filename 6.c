//strcmp
#include <stdio.h>
int strcmpFunc(char st1[], char st2[]); //ANSI prototype

int main() {
    //driver
    char stringOne[1000];//max lentgh is 1000
    char stringTwo[1000];
    int sizeChecker=0, sizeCheckerTwo=0;//size of each one
    char sampleChar;
    //getting input from user
    while((sampleChar = getchar()) != '\n' && sampleChar != EOF){
        stringOne[sizeChecker] = sampleChar;
        sizeChecker++;
    }
    stringOne[sizeChecker] = '\0';//adding null char

    while((sampleChar = getchar()) != '\n' && sampleChar != EOF){
        stringTwo[sizeCheckerTwo] = sampleChar;
        sizeCheckerTwo++;
    }
    stringTwo[sizeCheckerTwo] = '\0';

    printf("%d\n", strcmpFunc(stringOne, stringTwo));//func call

    
    return 0;
}
  

int strcmpFunc(char st1[], char st2[]){//func def

    int indexOne,indexTwo,sampleCharTwo;

    for (indexOne = 0;indexOne < 1000;indexOne++){//size of first character array
        if (st1[indexOne] == '\0'){
            break;
        }
    }
    
    for (indexTwo = 0;indexTwo < 1000; indexTwo++){//size of second character array
        if (st2[indexTwo] == '\0'){
            break;
        }
    }

    if (indexOne!=indexTwo){
        return -1;
    }
    else{
        for(sampleCharTwo = 0;sampleCharTwo < indexOne;sampleCharTwo++){
            if (st1[sampleCharTwo] != st2[sampleCharTwo]){
                return -1;
            }   
        }return 1;    
    }
    return -1;   
}