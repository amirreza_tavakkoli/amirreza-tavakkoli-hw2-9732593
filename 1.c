/*Gets a number as an array of chars and returns back 
 array of least possible number that could be created*/
#include <stdio.h>
char * sortingFunction(char array_p[], int lent); //ANSI prototype

//---------------------------------------------------------//

int main() {//driver for given function
char firstArray[1000];//array with max length of 1000
int length=0;//length
char character;
//getting the input
while((character=getchar()) != '\n' && character != EOF){
    firstArray[length] = character;
    length++;
}

firstArray[length]= '\0' ;//adding null char
printf("%s",sortingFunction(firstArray, length));//func call

    return 0;
}

  //----------------------------------------------------------//

//function def
char * sortingFunction(char array_p[], int lent){
int indexOne, indexTwo, sizeChecker;
char sampleChar = 'c';


//input validation
for(sizeChecker=0;sizeChecker<lent;sizeChecker++){
    if(array_p[sizeChecker] >= 59 || array_p[sizeChecker] <= 46){
        return "CANT USE NONDIGITS HERE";
    }
}


for(indexOne = 0;indexOne < lent;indexOne++){
    for(indexTwo = 0;indexTwo < (lent - 1);indexTwo++){
        if(array_p[indexTwo] > array_p[indexTwo + 1]){
            sampleChar = array_p[indexTwo];
            array_p[indexTwo] = array_p[indexTwo + 1];
            array_p[indexTwo + 1]=sampleChar;
            indexTwo = 0;
        }
            
    }
    indexTwo = 0;
}

return array_p;
}