//subarray checker
#include <stdio.h>
int strcmpFunc(char st1[], char st2[]); //ANSI prototype
//-----------------------------------------//
int main() {
    //driver
    char firstString[1000];
    char secondString[1000];
    int sizeOfFirstString=0, sizeOfSecondString = 0;
    char sampleChar;
    printf("second string must not be greater than first one\n");
    //getting inputs
    while((sampleChar = getchar()) != '\n' && sampleChar != EOF){
        firstString[sizeOfFirstString] = sampleChar;
        sizeOfFirstString++;
    }
    firstString[sizeOfFirstString] = '\0';

    while((sampleChar=getchar()) != '\n' && sampleChar != EOF){
        secondString[sizeOfSecondString] = sampleChar;
        sizeOfSecondString++;
    }
    secondString[sizeOfSecondString] = '\0';

    printf("%d\n", strcmpFunc(firstString, secondString));//func call

    return 0;
}
  
//-------------------------------------------//
int strcmpFunc(char st1[], char st2[]){
    int indexOne, indexTwo, lengthOfStringOne, leghtOfStringTwo;
    int levelChecker = 0;
    int ilevel;
    //length
    for (indexOne=0;indexOne < 1000;indexOne++){
        if (st1[indexOne] == '\0'){
           break;
       }
    }
    
    for (indexTwo=0;indexTwo < 1000;indexTwo++){
      if (st2[indexTwo] == '\0'){
         break;
      }
    }

    lengthOfStringOne = indexOne;
    leghtOfStringTwo = indexTwo;
    indexOne = 0;
    indexTwo = 0;

//length legitimacy checker
    if (leghtOfStringTwo > lengthOfStringOne){
       printf("second string is greater than first one\n");
      return 0;
    }
    if (leghtOfStringTwo == 0){
      return 0;
    }

    for (indexOne = 0;indexOne < lengthOfStringOne;indexOne++){
         ilevel=indexOne;
        for (indexTwo = 0;indexTwo < lengthOfStringOne;){
            if (st2[indexOne] == st1[indexTwo]){
                levelChecker++;
                if (levelChecker >= leghtOfStringTwo){
                    return 1;
                }
            indexOne++;
            indexTwo++;
            }
            else if (indexOne != ilevel){
                levelChecker = 0;
                indexOne = ilevel;
            }
            else {
                indexTwo++;
                levelChecker=0;
            }
            if (levelChecker >= leghtOfStringTwo){
                return 1;
            }
        }
        indexTwo = 0;
    }
return 0;
}