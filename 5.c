//sorting string
#include <stdio.h>
const char *sort1(char ar[], int lent);//ANSI prototype
//-----------------------------------------------------/
int main() {//driver code
    char arrayOne[1000];//max length
    int sizeOfArray = 0;
    char sampleChar;
    //input
    while((sampleChar = getchar()) != '\n' && sampleChar != EOF)
    {arrayOne[sizeOfArray] = sampleChar;
    sizeOfArray++;
    }
    arrayOne[sizeOfArray] = '\0';//adding null char
    printf("%s", sort1(arrayOne, sizeOfArray));

    return 0;
}
  
//--------------------------------------------------/
const char *sort1(char ar[], int lent){
    int indexOne, indexTwo;
    char sampleChar='c';

    for(indexOne = 0;indexOne < lent;indexOne++){
        for(indexTwo = 0;indexTwo < (lent - 1);indexTwo++){
            if(ar[indexTwo] > ar[indexTwo + 1]){
                sampleChar = ar[indexTwo];
                ar[indexTwo] = ar[indexTwo + 1];
                ar[indexTwo+1] = sampleChar;
                indexTwo = 0;
            }
        }
        indexTwo = 0;
    }
    return ar;

}